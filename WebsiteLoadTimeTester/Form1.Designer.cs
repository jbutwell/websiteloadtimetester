﻿namespace WebsiteLoadTimeTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.URLBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.getLoadTimes = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numTries = new System.Windows.Forms.NumericUpDown();
            this.intTries = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.buttonStop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numTries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTries)).BeginInit();
            this.SuspendLayout();
            // 
            // URLBox
            // 
            this.URLBox.Location = new System.Drawing.Point(44, 49);
            this.URLBox.Name = "URLBox";
            this.URLBox.Size = new System.Drawing.Size(467, 20);
            this.URLBox.TabIndex = 0;
            this.URLBox.Text = "http://";
            this.URLBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.URLBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(321, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Enter URL Here (must be fully formed, ex: http://www.google.com)";
            // 
            // getLoadTimes
            // 
            this.getLoadTimes.Location = new System.Drawing.Point(286, 78);
            this.getLoadTimes.Name = "getLoadTimes";
            this.getLoadTimes.Size = new System.Drawing.Size(111, 55);
            this.getLoadTimes.TabIndex = 3;
            this.getLoadTimes.Text = "Get Loading Times";
            this.getLoadTimes.UseVisualStyleBackColor = true;
            this.getLoadTimes.Click += new System.EventHandler(this.getLoadTimes_Click);
            // 
            // outputBox
            // 
            this.outputBox.Location = new System.Drawing.Point(12, 149);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.ReadOnly = true;
            this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputBox.Size = new System.Drawing.Size(538, 476);
            this.outputBox.TabIndex = 3;
            this.outputBox.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Number of Tries";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Interval between Tries";
            // 
            // numTries
            // 
            this.numTries.Location = new System.Drawing.Point(134, 82);
            this.numTries.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTries.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numTries.Name = "numTries";
            this.numTries.Size = new System.Drawing.Size(120, 20);
            this.numTries.TabIndex = 1;
            this.numTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTries.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // intTries
            // 
            this.intTries.Location = new System.Drawing.Point(134, 108);
            this.intTries.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.intTries.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.intTries.Name = "intTries";
            this.intTries.Size = new System.Drawing.Size(120, 20);
            this.intTries.TabIndex = 2;
            this.intTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.intTries.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(254, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "(s)";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Location = new System.Drawing.Point(409, 78);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(111, 55);
            this.buttonStop.TabIndex = 11;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 637);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.intTries);
            this.Controls.Add(this.numTries);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.getLoadTimes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.URLBox);
            this.Name = "Form1";
            this.Text = "Website Load Time Tester";
            ((System.ComponentModel.ISupportInitialize)(this.numTries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intTries)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox URLBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button getLoadTimes;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numTries;
        private System.Windows.Forms.NumericUpDown intTries;
        private System.Windows.Forms.Label label4;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button buttonStop;
    }
}

