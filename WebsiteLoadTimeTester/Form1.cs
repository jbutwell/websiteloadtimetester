﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Cache;
using System.IO;
using System.Diagnostics;

namespace WebsiteLoadTimeTester
{
    public partial class Form1 : Form
    {


        public WebRequest wrGetPage;
        public Stream stData;
        public StreamReader srDataReader;
        public string returnedData;
        public int tries;
        public int interval;
        public string testURL;
        public Stopwatch elapsedTime = new Stopwatch();
        public Stopwatch intervalTime = new Stopwatch();



        public Form1()
        {
            InitializeComponent();
        }

        private void getLoadTimes_Click(object sender, EventArgs e)
        {

            tries = (int)numTries.Value;
            interval = (int)intTries.Value;
            testURL = URLBox.Text;


            if (intTries.Value < 5)
            {
                MessageBox.Show("Please set an interval greater than 5. Less than 5 is malicious!");
            }

            else if (testURL.Length < 8)
            {
                MessageBox.Show("You have not entered a fully formed URL, please try again (hint: you need to be checking something on the http:// or https:// protocol)");
            }

            else if ((testURL.Substring(0, 7) != "http://") && (testURL.Substring(0, 8) != "https://"))
            {
                MessageBox.Show("You have not entered a fully formed URL, please try again (hint: you need to be checking something on the http:// or https:// protocol)");
            }

            else
            {

                backgroundWorker1.RunWorkerAsync();

                getLoadTimes.Enabled = false;
                numTries.Enabled = false;
                intTries.Enabled = false;
                URLBox.ReadOnly = true;
                buttonStop.Enabled = true;
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.WorkerSupportsCancellation = true;
            GetPageLoadTimes();

        }

        

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            outputBox.AppendText(e.UserState as String);
            
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            outputBox.AppendText(Environment.NewLine + Environment.NewLine + Environment.NewLine);
            getLoadTimes.Enabled = true;
            numTries.Enabled = true;
            intTries.Enabled = true;
            URLBox.ReadOnly = false;
            buttonStop.Enabled = false;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
            }
        }


        public void GetPageLoadTimes()
        {
            tries = (int)numTries.Value;
            interval = (int)intTries.Value;
            testURL = URLBox.Text;


            backgroundWorker1.ReportProgress(0, DateTime.Now + ", " + testURL + Environment.NewLine + Environment.NewLine);
            for (int i = 0; i < tries; i++)
            {
                if (backgroundWorker1.CancellationPending == true)
                {
                    break;
                }
                else
                {
                    try
                    {
                        elapsedTime = Stopwatch.StartNew();
                        wrGetPage = WebRequest.Create(testURL);
                        stData = wrGetPage.GetResponse().GetResponseStream();
                        wrGetPage.GetResponse().Close();
                        elapsedTime.Stop();
                        backgroundWorker1.ReportProgress((i / tries * 100), DateTime.Now + ", Load Attempt #" + (i + 1) + ": " + TimeSpan.FromMilliseconds(elapsedTime.ElapsedMilliseconds).TotalSeconds + "(s)" + Environment.NewLine);

                    }
                    catch (Exception exception)
                    {
                        backgroundWorker1.ReportProgress((i / tries * 100), exception.ToString() + Environment.NewLine);
                        
                    }

                    for (int y = 0; y < 1000; y++)
                    {

                        if (backgroundWorker1.CancellationPending == true)
                        {
                            backgroundWorker1.ReportProgress((i / tries * 100), Environment.NewLine + "Test stopped by user" + Environment.NewLine + Environment.NewLine);
                            break;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(interval);
                        }
                    }
                    
                }

            }
            
        }

        private void URLBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!getLoadTimes.Enabled)
                {
                    buttonStop.PerformClick();
                }
                else
                {
                    getLoadTimes.PerformClick();
                }
            }
        }

        

        

    }
}
